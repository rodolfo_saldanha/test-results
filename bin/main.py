from data_loader import file_loader
from data_analysis import data_preparation
from data_analysis import result_analysis


def execute_pipeline():
    # Reads the files in directory 'data/'
    raw_data = file_loader.read_directory("data/")
    # Creates the features needed for the analysis
    prepared_data = data_preparation.add_extra_features(raw_data)
    # Generates the reports in the folder 'results/'
    result_analysis.generate_analysis(prepared_data)


if __name__ == "__main__":
    execute_pipeline()
