# Automated Testing Essential Concepts

Answer to the Exercises on [Issue #23](https://gitlab.com/DareData-open-source/tutorials/-/issues/23#exercises). The goal is to implement a data analysis pipeline of test results better explained in the section just mentioned.

## How to use

In order to run the pipeline, you need to execute `python main.py` inside `bin/`. If you want to run the tests, you need to run `pytest tests/` inside the root directory.

- `bin/` contains the main file.
- `data/` is the data source and it contains .csv files.
- `data_loader/` is the package for loading the data.
- `data_analysis/` is the package that takes care of preparing the data and generating results.
- `results/` is the destination folder of the results generated after analysis.
- `tests/` contains the integration and unit tests. The file `unit_test_after_change.py` should only be run after the required change is implemented.

## Requirements

Execute `requirements.txt` with `pip install -r requirements.txt`, and you will have all packages required in this project.
