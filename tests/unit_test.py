import os
import unittest

from data_loader import file_loader
from data_analysis import data_preparation
from data_analysis import result_analysis


class TestUnits(unittest.TestCase):

    def test_read_directory(self):
        """
        Checks if the files were read correctly.
        """
        raw_data = file_loader.read_directory("data/")

        self.assertTrue(len(raw_data.columns) == 4)
        self.assertFalse(raw_data.empty)

    def test_calculate_score(self):
        """
        Checks if the score was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        data_add_feat_1 = data_preparation.calculate_age(raw_data, "birth")
        prepared_data = data_preparation.calculate_score(
            data_add_feat_1,
            "correct_answers",
            "age")

        self.assertTrue(len(prepared_data[prepared_data.score.isnull()]) == 0)
        self.assertTrue(len(prepared_data[prepared_data.score < 0]) == 0)
        self.assertTrue(len(prepared_data[prepared_data.score > 100]) == 0)

    def test_create_user(self):
        """
        Checks if the user was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        prepared_data = data_preparation.create_user(raw_data,
                                                     "name",
                                                     "school")

        self.assertTrue(len(prepared_data[prepared_data.user.isnull()]) == 0)
        self.assertTrue(len(prepared_data[
            prepared_data["user"].str.find("@") >= 0]) == len(prepared_data))

    def test_student_approved(self):
        """
        Checks if the student_approved.csv file was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        prepared_data = data_preparation.add_extra_features(raw_data)
        result_analysis.student_approved(prepared_data)
        file = "results/student_approved.csv"
        self.assertTrue(os.stat(file).st_size != 0)

    def test_student_failed(self):
        """
        Checks if the student_failed.csv file was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        prepared_data = data_preparation.add_extra_features(raw_data)
        result_analysis.student_failed(prepared_data)
        file = "results/student_failed.csv"
        self.assertTrue(os.stat(file).st_size != 0)

    def test_school_approved(self):
        """
        Checks if the school_approved.csv file was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        prepared_data = data_preparation.add_extra_features(raw_data)
        result_analysis.school_approved(prepared_data)
        file = "results/school_approved.csv"
        self.assertTrue(os.stat(file).st_size != 0)

    def test_school_failed(self):
        """
        Checks if the school_failed.csv file was correctly generated.
        """
        raw_data = file_loader.read_directory("data/")
        prepared_data = data_preparation.add_extra_features(raw_data)
        result_analysis.school_failed(prepared_data)
        file = "results/school_failed.csv"
        self.assertTrue(os.stat(file).st_size != 0)
