import glob
import os
import unittest

from data_loader import file_loader
from data_analysis import data_preparation
from data_analysis import result_analysis


class TestIntegration(unittest.TestCase):

    def setUp(self):
        """
        Sets up all the Dataframes used in the pipeline
        """
        self.raw_data = file_loader.read_directory("data/")
        self.prepared_data = data_preparation.add_extra_features(self.raw_data)
        result_analysis.generate_analysis(self.prepared_data)

    def test_results_generation(self):
        """
        Checks if the results were actually generated and
        ensures they have the correct number of columns.
        """
        self.assertFalse(self.raw_data.empty)
        self.assertTrue(len(self.raw_data.columns) == 4)
        self.assertFalse(self.prepared_data.empty)
        self.assertTrue(len(self.prepared_data.columns) == 7)

        file_count = 0
        for file in glob.glob("results/" + '*.csv'):
            if file.endswith(".csv"):
                file_count = file_count + 1

        self.assertTrue(file_count == 4)

    def test_results_content(self):
        """
        Checks if the content in the result files are correct.
        """
        for file in glob.glob("results/" + '*.csv'):
            if file.endswith(".csv"):
                assert os.stat(file).st_size != 0

        no_na_prepared_data = self.prepared_data.dropna()
        assert len(self.prepared_data) == len(no_na_prepared_data)
