import os

from data_loader import file_loader
from data_analysis import data_preparation
from data_analysis import result_analysis


def test_create_user():
    raw_data = file_loader.read_directory("data/")
    prepared_data = data_preparation.create_user(raw_data,
                                                 "name",
                                                 "school")

    assert len(prepared_data[prepared_data.user.isnull()]) == 0
    assert len(prepared_data[
        prepared_data["user"].str.find("/") >= 0]) == len(prepared_data)
