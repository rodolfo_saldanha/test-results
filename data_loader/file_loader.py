import pandas as pd
import glob


def read_file(file):
    '''Reads file containing the correct number of columns.

    Parameters:
    file (string): Data file.

    Returns:
    raw_data (DataFrame): DataFrame containing the data read.
    '''
    raw_data = pd.read_csv(file, sep=";")

    if len(raw_data.columns) == 4:
        print("File loaded correctly.")
        return raw_data
    else:
        print("Wrong file.")
        return pd.DataFrame()


def read_directory(directory):
    '''Iterates through the data source directory.

    Parameters:
    directory (string): Directory containing the data files.

    Returns:
    raw_data (DataFrame): DataFrame containing all the data read among the
    files.
    '''
    raw_data = pd.DataFrame()
    for file in glob.glob(directory + '*.csv'):
        if file.endswith(".csv"):
            raw_file = read_file(file)
            raw_data = pd.concat([raw_data, raw_file],
                                 axis=0,
                                 ignore_index=True)
    print("All files read successfully.")

    return raw_data


if __name__ == "__main__":
    raw_data = read_directory("data/")
