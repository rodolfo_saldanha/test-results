import os


def student_approved(data):
    '''Generates a .csv of students above the minimum score.

    Parameters:
    data (DataFrame): DataFrame with the student's data.
    '''
    data.loc[data['score'] > 30].to_csv(
        "results/student_approved.csv",
        sep=";")


def student_failed(data):
    '''Generates a .csv of students below the minimum score.

    Parameters:
    data (DataFrame): DataFrame with the student's data.
    '''
    data.loc[data['score'] <= 30].to_csv(
        "results/student_failed.csv",
        sep=";")


def school_approved(data):
    '''Generates a .csv of schools above the minimum score.

    Parameters:
    data (DataFrame): DataFrame with the student's data.
    '''
    data_results = data.groupby("school")["score"].mean().reset_index()
    data_results.columns = ["school", "avg_score"]
    data_results.loc[data_results['avg_score'] > 30].to_csv(
        "results/school_approved.csv",
        sep=";")


def school_failed(data):
    '''Generates a .csv of schools below the minimum score.

    Parameters:
    data (DataFrame): DataFrame with the student's data.
    '''
    data_results = data.groupby("school")["score"].mean().reset_index()
    data_results.columns = ["school", "avg_score"]
    data_results.loc[data_results['avg_score'] <= 30].to_csv(
        "results/school_failed.csv",
        sep=";")


def generate_analysis(data):
    '''Creates the results directory and add the reports generated.

    Parameters:
    data (DataFrame): DataFrame with the student's data.
    '''
    if not os.path.exists('results'):
        os.makedirs('results')
    student_approved(data)
    student_failed(data)
    school_approved(data)
    school_failed(data)


if __name__ == "__main__":
    student_approved = students_approved(data)
    failed = student_failed(data)
