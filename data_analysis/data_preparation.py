import pandas as pd


def calculate_age(df, birth_column):
    '''Calculates the age of the student.

    Parameters:
    df (DataFrame): DataFrame with the student's data.
    birth_column (string): Name of the birth column.

    Returns:
    df (DataFrame): Original DataFrame plus the age column.
    '''
    now = pd.Timestamp("now")
    df[birth_column] = df[birth_column].apply(
        lambda x: pd.to_datetime(x, format="%d/%m/%Y"))
    df["age"] = (now - df[birth_column]).astype("timedelta64[Y]")
    return df


def calculate_score(df, correct_answers_column, age_column):
    '''Calculates the score of the student.

    Parameters:
    df (DataFrame): DataFrame with the student's data.
    correct_answers_column (string): Name of the correct answers column.
    age_column (string): Name of the age column.

    Returns:
    df (DataFrame): Original DataFrame plus the score column.
    '''
    index = 1 - ((df[age_column] - 10) / 10)
    df["score"] = df[correct_answers_column] * index
    return df


def create_user(df, name_column, school_column):
    '''Created the student use.

    Parameters:
    df (DataFrame): DataFrame with the student's data.
    name_column (string): Name of the name column.
    school_column (string): Name of the school column.

    Returns:
    df (DataFrame): Original DataFrame plus the user column.
    '''
    df['user'] = df[name_column] + "@" + df[school_column]
    # REQUIRED CHANGE
    # df['user'] = df[name_column] + "/" + df[school_column]
    return df


def add_extra_features(raw_data,
                       birth_column="birth",
                       correct_answers_column="correct_answers",
                       age_column="age",
                       name_column="name",
                       school_column="school"):
    '''Creates all the new features.

    Parameters:
    raw_data (DataFrame): DataFrame with the student's data.
    birth_column (string): Name of the birthday column.
    correct_answers_column (string): Name of the correct answers column.
    age_column (string): Name of the age column.
    name_column (string): Name of the name column.
    school_column (string): Name of the school column.

    Returns:
    data_add_feat_3 (DataFrame): DataFrame with the student's
    data and new three features.
    '''
    raw_data_copy = raw_data.copy()
    data_add_feat_1 = calculate_age(raw_data_copy, birth_column)
    data_add_feat_2 = calculate_score(
        data_add_feat_1,
        correct_answers_column,
        age_column)
    data_add_feat_3 = create_user(
        data_add_feat_2,
        name_column,
        school_column)

    return data_add_feat_3


if __name__ == "__main__":
    add_extra_features(raw_data)
